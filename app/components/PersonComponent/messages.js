/*
 * PersonComponent Messages
 *
 * This contains all the text for the PersonComponent component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.PersonComponent';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the PersonComponent component!',
  },
});
